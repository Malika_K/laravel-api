<?php

namespace App\Http\Requests;

use App\Models\ProductCategory;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateProductCategoryRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('product_category_access');
    }

    public function rules()
    {
        return [
            'name'       => [
                'string',
                'required',
            ],
            'photo'      => [
                'array',
                'nullable',
            ],
            'photo.*.id' => [
                'integer',
                'exists:media,id',
            ],
            'parent_id'  => [
                'integer',
                'exists:product_categories,id',
                'nullable',
            ],
        ];
    }
}