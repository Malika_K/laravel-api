<?php

namespace App\Http\Requests;

use App\Models\ProductManufacturer;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreProductManufacturerRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('product_manufacturer_access');
    }

    public function rules()
    {
        return [
            'name'       => [
                'string',
                'min:0',
                'max:100',
                'required',
            ],
            'photo'      => [
                'array',
                'nullable',
            ],
            'photo.*.id' => [
                'integer',
                'exists:media,id',
            ],
        ];
    }
}