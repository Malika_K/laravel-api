<?php

namespace App\Http\Requests;

use App\Models\ProductDetail;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateProductDetailRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('product_access');
    }

    public function rules()
    {
        return [
            'name'       => [
                'string',
                'min:0',
                'max:50',
                'required',
            ],
            'value'      => [
                'string',
                'min:0',
                'max:100',
                'required',
            ],
            'product_id' => [
                'integer',
                'exists:products,id',
                'required',
            ],
        ];
    }
}