<?php

namespace App\Http\Requests;

use App\Models\Information;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;

class StoreInformationRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('information_access');
    }

    public function rules()
    {
        return [
            'title'      => [
                'string',
                'required',
            ],
            'text'       => [
                'string',
                'required',
            ],
            'photo'      => [
                'array',
                'nullable',
            ],
            'photo.*.id' => [
                'integer',
                'exists:media,id',
            ],
            'block_type' => [
                'nullable',
                'in:' . implode(',', Arr::pluck(Information::BLOCK_TYPE_SELECT, 'value')),
            ],
        ];
    }
}