<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProductDetailRequest;
use App\Http\Requests\UpdateProductDetailRequest;
use App\Http\Resources\Admin\ProductDetailResource;
use App\Models\Product;
use App\Models\ProductDetail;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ProductDetailApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('product_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ProductDetailResource(ProductDetail::with(['product'])->advancedFilter());
    }

    public function store(/*Request*/StoreProductDetailRequest $request)
    {
        $productDetail = ProductDetail::create($request->validated());
        return (new ProductDetailResource($productDetail))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function create(ProductDetail $productDetail)
    {
        abort_if(Gate::denies('product_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'meta' => [
                'product' => Product::get(['id', 'name']),
            ],
        ]);
    }

    public function show(ProductDetail $productDetail)
    {
        abort_if(Gate::denies('product_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ProductDetailResource($productDetail->load(['product']));
    }

    public function update(UpdateProductDetailRequest $request, ProductDetail $productDetail)
    {
        $productDetail->update($request->validated());

        return (new ProductDetailResource($productDetail))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function edit(ProductDetail $productDetail)
    {
        abort_if(Gate::denies('product_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'data' => new ProductDetailResource($productDetail->load(['product'])),
            'meta' => [
                'product' => Product::get(['id', 'name']),
            ],
        ]);
    }

    public function destroy(ProductDetail $productDetail)
    {
        abort_if(Gate::denies('product_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $productDetail->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}