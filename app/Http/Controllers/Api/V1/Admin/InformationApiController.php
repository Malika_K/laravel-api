<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreInformationRequest;
use App\Http\Requests\UpdateInformationRequest;
use App\Http\Resources\Admin\InformationResource;
use App\Models\Information;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class InformationApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('information_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new InformationResource(Information::advancedFilter());
    }

    public function store(StoreInformationRequest $request)
    {
        $information = Information::create($request->validated());

        if ($media = $request->input('photo', [])) {
            Media::whereIn('id', data_get($media, '*.id'))
                ->where('model_id', 0)
                ->update(['model_id' => $information->id]);
        }

        return (new InformationResource($information))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function create(Information $information)
    {
        abort_if(Gate::denies('information_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'meta' => [
                'block_type' => Information::BLOCK_TYPE_SELECT,
            ],
        ]);
    }

    public function show(Information $information)
    {
        abort_if(Gate::denies('information_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new InformationResource($information);
    }

    public function update(UpdateInformationRequest $request, Information $information)
    {
        $information->update($request->validated());

        $information->updateMedia($request->input('photo', []), 'information_photo');

        return (new InformationResource($information))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function edit(Information $information)
    {
        abort_if(Gate::denies('information_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'data' => new InformationResource($information),
            'meta' => [
                'block_type' => Information::BLOCK_TYPE_SELECT,
            ],
        ]);
    }

    public function destroy(Information $information)
    {
        abort_if(Gate::denies('information_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $information->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeMedia(Request $request)
    {
        abort_if(Gate::none(['information_access']), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->has('size')) {
            $this->validate($request, [
                'file' => 'max:' . $request->input('size') * 1024,
            ]);
        }

        $model         = new Information();
        $model->id     = $request->input('model_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('file')->toMediaCollection($request->input('collection_name'));

        return response()->json($media, Response::HTTP_CREATED);
    }
}