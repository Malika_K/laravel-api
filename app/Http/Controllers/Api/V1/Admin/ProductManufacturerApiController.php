<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProductManufacturerRequest;
use App\Http\Requests\UpdateProductManufacturerRequest;
use App\Http\Resources\Admin\ProductManufacturerResource;
use App\Models\ProductManufacturer;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class ProductManufacturerApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('product_manufacturer_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ProductManufacturerResource(ProductManufacturer::advancedFilter());
    }

    public function store(StoreProductManufacturerRequest $request)
    {
        $productManufacturer = ProductManufacturer::create($request->validated());

        if ($media = $request->input('photo', [])) {
            Media::whereIn('id', data_get($media, '*.id'))
                ->where('model_id', 0)
                ->update(['model_id' => $productManufacturer->id]);
        }

        return (new ProductManufacturerResource($productManufacturer))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function create(ProductManufacturer $productManufacturer)
    {
        abort_if(Gate::denies('product_manufacturer_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'meta' => [],
        ]);
    }

    public function show(ProductManufacturer $productManufacturer)
    {
        abort_if(Gate::denies('product_manufacturer_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ProductManufacturerResource($productManufacturer);
    }

    public function update(UpdateProductManufacturerRequest $request, ProductManufacturer $productManufacturer)
    {
        $productManufacturer->update($request->validated());

        $productManufacturer->updateMedia($request->input('photo', []), 'product_manufacturer_photo');

        return (new ProductManufacturerResource($productManufacturer))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function edit(ProductManufacturer $productManufacturer)
    {
        abort_if(Gate::denies('product_manufacturer_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return response([
            'data' => new ProductManufacturerResource($productManufacturer),
            'meta' => [],
        ]);
    }

    public function destroy(ProductManufacturer $productManufacturer)
    {
        abort_if(Gate::denies('product_manufacturer_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $productManufacturer->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeMedia(Request $request)
    {
        abort_if(Gate::none(['product_manufacturer_access']), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->has('size')) {
            $this->validate($request, [
                'file' => 'max:' . $request->input('size') * 1024,
            ]);
        }

        $model         = new ProductManufacturer();
        $model->id     = $request->input('model_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('file')->toMediaCollection($request->input('collection_name'));

        return response()->json($media, Response::HTTP_CREATED);
    }
}