<?php

namespace App\Support;

trait HasAdvancedFilter
{
    public function scopeAdvancedFilter($query)
    {
        return $this->processQuery($query, request()->all())
            ->when(request('product_id'), function($query) {
                $query->where('product_id', '=', request('product_id'));
            })
            ->when(request('category'), function ($query) {
                $query->whereHas('category', function($q) {
                    $q->where('product_category_id', '=', request('category'));
                });             
            }) 
            ->when(count(request('subcategories', [])), function ($query) {
                $query->whereHas('category', function($q) {
                    $q->whereIn('product_category_id', request('subcategories'));
                });
            })
            ->when(request('manufacturers'), function ($query) {
                $query->whereIn('manufacturer_id', request('manufacturers'));
            })
            ->orderBy(request('sort', 'id'), request('order', 'desc'))
            ->paginate(request('limit', 10));       
    }

    public function processQuery($query)
    {
        return $query;
    }
}
