<?php

namespace App\Models;

use App\Support\HasAdvancedFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use \DateTimeInterface;

class Information extends Model implements HasMedia
{
    use HasAdvancedFilter, SoftDeletes, InteractsWithMedia, HasFactory;

    public $table = 'information';

    protected $appends = [
        'photo',
        'block_type_label',
    ];

    protected $orderable = [
        'id',
        'title',
        'block_type',
    ];

    protected $filterable = [
        'id',
        'title',
        'block_type',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'title',
        'text',
        'block_type',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const BLOCK_TYPE_SELECT = [
        [
            'label' => 'О нас',
            'value' => 'about',
        ],
        [
            'label' => 'Контакты',
            'value' => 'contacts',
        ],
        [
            'label' => 'График работы',
            'value' => 'schedule',
        ],
        [
            'label' => 'Разрешение на покупку оружия',
            'value' => 'license',
        ],
        [
            'label' => 'Приветствие',
            'value' => 'welcome',
        ],
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $thumbnailWidth  = 50;
        $thumbnailHeight = 50;

        $thumbnailPreviewWidth  = 120;
        $thumbnailPreviewHeight = 120;

        $this->addMediaConversion('thumbnail')
            ->width($thumbnailWidth)
            ->height($thumbnailHeight)
            ->fit('crop', $thumbnailWidth, $thumbnailHeight);
        $this->addMediaConversion('preview_thumbnail')
            ->width($thumbnailPreviewWidth)
            ->height($thumbnailPreviewHeight)
            ->fit('crop', $thumbnailPreviewWidth, $thumbnailPreviewHeight);
    }

    public function getPhotoAttribute()
    {
        return $this->getMedia('information_photo')->map(function ($item) {
            $media                      = $item->toArray();
            $media['url']               = $item->getUrl();
            $media['thumbnail']         = $item->getUrl('thumbnail');
            $media['preview_thumbnail'] = $item->getUrl('preview_thumbnail');

            return $media;
        });
    }

    public function getBlockTypeLabelAttribute()
    {
        return collect(static::BLOCK_TYPE_SELECT)->firstWhere('value', $this->block_type)['label'] ?? '';
    }
}