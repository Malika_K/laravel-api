<?php

namespace App\Models;

use App\Support\HasAdvancedFilter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Image\Manipulations;
use \DateTimeInterface;

class Product extends Model implements HasMedia
{
    use HasAdvancedFilter, SoftDeletes, InteractsWithMedia, HasFactory;

    public $table = 'products';

    protected $appends = [
        'photo',
    ];

    protected $casts = [
        'available' => 'boolean',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $orderable = [
        'id',
        'name',
        'description',
        'price',
        'available',
        'manufacturer.name',
    ];

    protected $filterable = [
        'id',
        'name',
        'description',
        'price',
        'category.name',
        'manufacturer.name',
    ];

    protected $fillable = [
        'name',
        'description',
        'price',
        'available',
        'manufacturer_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $thumbnailWidth  = 50;
        $thumbnailHeight = 50;

        $thumbnailPreviewWidth  = 350;
        $thumbnailPreviewHeight = 350;

        $this->addMediaConversion('thumbnail')
            ->width($thumbnailWidth)
            ->height($thumbnailHeight)
            ->fit('crop', $thumbnailWidth, $thumbnailHeight);
        $this->addMediaConversion('preview_thumbnail')
            ->width($thumbnailPreviewWidth)
            ->height($thumbnailPreviewHeight)
            ->fit(Manipulations::FIT_FILL, $thumbnailPreviewWidth, $thumbnailPreviewHeight)
            ->background('ffffff');           
    }

    public function getPhotoAttribute()
    {
        return $this->getMedia('product_photo')->map(function ($item) {
            $media                      = $item->toArray();
            $media['url']               = $item->getUrl();
            $media['thumbnail']         = $item->getUrl('thumbnail');
            $media['preview_thumbnail'] = $item->getUrl('preview_thumbnail');

            return $media;
        });
    }

    public function category()
    {
        return $this->belongsToMany(ProductCategory::class);
    }

    public function manufacturer()
    {
        return $this->belongsTo(ProductManufacturer::class);
    }

    public function product_detail()
    {
        return $this->hasMany(ProductDetail::class, 'product_id');
    }
    
}