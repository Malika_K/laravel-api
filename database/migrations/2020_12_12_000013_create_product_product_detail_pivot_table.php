<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductProductDetailPivotTable extends Migration
{
    public function up()
    {
        Schema::create('product_product_detail', function (Blueprint $table) {
            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id', 'product_id_fk_2778086')->references('id')->on('products')->onDelete('cascade');
            $table->unsignedBigInteger('product_detail_id');
            $table->foreign('product_detail_id', 'product_detail_id_fk_2778086')->references('id')->on('product_details')->onDelete('cascade');
        });
    }
}