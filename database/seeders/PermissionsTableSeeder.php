<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ], 
            [
                'id'    => 2,
                'title' => 'permission_access',
            ],

            [
                'id'    => 3,
                'title' => 'role_access',
            ],
            [
                'id'    => 4,
                'title' => 'user_access',
            ],
            [
                'id'    => 5,
                'title' => 'category_access',
            ],
            [
                'id'    => 6,
                'title' => 'information_access',
            ], 
            [
                'id'    => 7,
                'title' => 'product_category_access',
            ],

            [
                'id'    => 8,
                'title' => 'product_manufacturer_access',
            ],
            [
                'id'    => 9,
                'title' => 'product_access',
            ]            
        ];

        Permission::insert($permissions);
    }
}
