<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    // Abilities
    Route::apiResource('abilities', 'AbilitiesController', ['only' => ['index']]);

    // Locales
    Route::get('locales/languages', 'LocalesController@languages')->name('locales.languages');
    Route::get('locales/messages', 'LocalesController@messages')->name('locales.messages');

    // Permissions
    Route::resource('permissions', 'PermissionsApiController');

    // Roles
    Route::resource('roles', 'RolesApiController');

    // Users
    Route::resource('users', 'UsersApiController');

     // Product Categories
    Route::post('product-categories/media', 'ProductCategoryApiController@storeMedia')->name('product-categories.storeMedia');
    Route::resource('product-categories', 'ProductCategoryApiController');
 

    // Products
    Route::post('products/media', 'ProductApiController@storeMedia')->name('products.storeMedia');
    Route::resource('products', 'ProductApiController');

    // Product Details
    Route::resource('product-details', 'ProductDetailApiController');

    // Product Manufacturers
    Route::post('product-manufacturers/media', 'ProductManufacturerApiController@storeMedia')->name('product-manufacturers.storeMedia');
    Route::resource('product-manufacturers', 'ProductManufacturerApiController');

    // Information
    Route::post('information/media', 'InformationApiController@storeMedia')->name('information.storeMedia');
    Route::resource('information', 'InformationApiController');
});
